//
//  Int.swift
//  POCDeezer
//
//  Created by mac on 14/01/1401 AP.
//

import Foundation
extension Int {
    func secondsToHoursMinutesSeconds() -> String {
        let dcf = DateComponentsFormatter()
        dcf.allowedUnits = [.hour, .minute, .second]
        dcf.unitsStyle = .positional
        dcf.zeroFormattingBehavior = .pad
        return dcf.string(from: TimeInterval(self)) ?? ""
    }
}
