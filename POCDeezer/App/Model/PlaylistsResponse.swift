//
//  Playlist.swift
//  POCDeezer
//
//  Created by mac on 11/01/1401 AP.
//

struct PlaylistsResponse: Codable {
    var total : Int?
    var data : [Playlist]?
    var checksum: String?
}

struct Playlist: Codable{
    
    var id: Int?
    var title: String?
    var pictureMedium: String?
    
    enum CodingKeys : String, CodingKey {
        case pictureMedium = "picture_medium"
        case title = "title"
        case id = "id"
    }
}
