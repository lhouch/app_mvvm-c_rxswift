//
//  TracksResponse.swift
//  POCDeezer
//
//  Created by mac on 14/01/1401 AP.
//

// MARK: - TracksResponse
struct TracksResponse: Codable {
    let data: [Track]?
    let checksum: String?
    let total: Int?
}

// MARK: - Datum
struct Track: Codable {
    let id: Int?
    let title: String?
    let link: String?
    let duration: Int?
    let artist: Artist?
}

// MARK: - Artist
struct Artist: Codable {
    let id: Int
    let name: String
    
}
