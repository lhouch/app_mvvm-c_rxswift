//
//  PlaylistInfoResponse.swift
//  POCDeezer
//
//  Created by mac on 14/01/1401 AP.
//

struct PlaylistInfoResponse: Codable {
    var id : Int?
    var title: String?
    var description: String?
    var duration: Int?
    var creator: Creator?
    var picture_medium: String?
}

struct Creator: Codable{
    var id : Int?
    var name: String?
}
