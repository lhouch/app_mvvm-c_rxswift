//
//  CustomError.swift
//  POCDeezer
//
//  Created by mac on 12/01/1401 AP.
//

enum CustomError: Error {
    case error(message: String)
}
