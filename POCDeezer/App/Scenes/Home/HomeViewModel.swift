//
//  HomeViewModel.swift
//  POCDeezer
//
//  Created by mac on 11/01/1401 AP.
//

import RxSwift
import UIKit

class HomeViewModel {
    
    fileprivate let disposeBag = DisposeBag()
    private let mPOCDeezerService: POCDeezerAPI
    
    // events
    let playListsResponse = PublishSubject<PlaylistsResponse>()
    let responseError = PublishSubject<Error>()
    let isLoading = BehaviorSubject(value: true)
    
    let didSelectPlayList = PublishSubject<Playlist>()
    
    
    
    init(service: POCDeezerAPI) {
        self.mPOCDeezerService = service
        
        // Fetch playLists
        getPlayLists()
    }
    
    func getPlayLists() -> Void {
        isLoading.onNext(true)
        self.mPOCDeezerService
            .fetchPlayLists()
            .subscribe { result in
                self.playListsResponse.onNext(result)
            } onError: { error in
                self.responseError.onNext(error)
            }.disposed(by: disposeBag)
        
    }
}

