//
//  PlayListCell.swift
//  POCDeezer
//
//  Created by mac on 15/01/1401 AP.
//

import UIKit

class PlayListCell: UICollectionViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var picture: UIImageView!
    
    @IBOutlet weak var viewContent: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configUI()
    }
    
    func initCell(playList : Playlist){
        self.title.text = playList.title
        
        if let imageUrl = playList.pictureMedium{
            self.picture.load(url: URL(string: imageUrl)!)
        }
    }
    
   func configUI(){
       self.viewContent.layer.cornerRadius = 5
       self.viewContent.layer.masksToBounds = true
    }
}
