//
//  PlayListView.swift
//  POCDeezer
//
//  Created by mac on 10/01/1401 AP.
//

import UIKit
import RxSwift

class HomeViewController: UIViewController, Storyboarded {
    
    static var storyboard = AppStoryboard.main
    
    
    @IBOutlet weak var mCollectionView: UICollectionView!
    
    var mPlayLists : [Playlist] = []
    
    private let disposeBag = DisposeBag()
    
    var viewModel: HomeViewModel?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Show Playlists in 3 grid 3 columns
        mCollectionView.collectionViewLayout = GridLayout(numberOfColumns: 3, heightCell: 228, spacing: 5)
        setUpBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    private func setUpBindings() {
        
        viewModel?.playListsResponse
            .subscribe { result in
                
                if let playLists = result.element?.data, !playLists.isEmpty {
                    self.mPlayLists = playLists
                    self.mCollectionView.reloadData()
                } else {
                    self.errorLoading(message: "No data to show")
                }
                
                self.hideActivityIndicatoryInSuperview()
                
            }.disposed(by: disposeBag)
        
        viewModel?.responseError.subscribe{error in
            self.hideActivityIndicatoryInSuperview()
            self.errorLoading(message: error.element!.localizedDescription)
        }.disposed(by: disposeBag)
        
        viewModel?.isLoading
            .subscribe { isLoading in
                if isLoading.element! {
                    self.showActivityIndicatoryInSuperview()
                }
            }.disposed(by: disposeBag)
        
    }
    
    func errorLoading(message: String) {
        self.showAlertViewInSuperview("Error", message: message) {
            print(message)
        }
    }
    
}




extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "playListCell", for: indexPath) as! PlayListCell
        cell.initCell(playList: self.mPlayLists[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mPlayLists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel?.didSelectPlayList.onNext(self.mPlayLists[indexPath.row])
    }
}


