//
//  HomeCoordinator.swift
//  POCDeezer
//
//  Created by mac on 12/01/1401 AP.
//

import Foundation
import RxSwift

class HomeCoordinator: BaseCoordinator {
    
    private let viewModel: HomeViewModel
    private let disposeBag = DisposeBag()
    
    lazy var apiClient: POCDeezerService = {
        return POCDeezerService.shared
    }()
    
    
    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
    }
    
    override func start() {
        // Start Home View
        let viewController = HomeViewController.instantiate()
        viewController.viewModel = viewModel
        navigationController.isNavigationBarHidden = true
        navigationController.viewControllers = [viewController]
        
        setUpBindings()
    }
    
    func showPlayList(_ playList: Playlist) {
        // Init PlayList Info Coordinator
        let coordinator = PlayListCoordinator(viewModel: PlayListViewModel(service: apiClient, playList: playList))
        coordinator.navigationController = navigationController
        
        start(coordinator: coordinator)
    }
    
    
    private func setUpBindings() {
        viewModel.didSelectPlayList
            .subscribe(onNext: { [weak self] playlist in
                // Got to playList Info
                self?.showPlayList(playlist)
            })
            .disposed(by: disposeBag)
    }
}
