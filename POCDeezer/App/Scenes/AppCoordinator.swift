//
//  AppCoordinator.swift
//  POCDeezer
//
//  Created by mac on 12/01/1401 AP.
//

import UIKit

class AppCoordinator: BaseCoordinator {
    
    // MARK: - Properties
    let window: UIWindow?
    
    // MARK: - Coordinator
    init(window: UIWindow?) {
        self.window = window
    }
    
    lazy var apiClient: POCDeezerService = {
        return POCDeezerService.shared
    }()
    
    
    override func start() {
        guard let window = window else {
            return
        }
        
        
        window.makeKeyAndVisible()
        
        showHome()
    }
    
    private func showHome() {
        removeChildCoordinators()
        let coordinator = HomeCoordinator(viewModel: HomeViewModel(service: apiClient))
        coordinator.navigationController = BaseNavigationController()
        start(coordinator: coordinator)
        
        ViewControllerUtils.setRootViewController(
            window: window!,
            viewController: coordinator.navigationController,
            withAnimation: true)
    }
}
