//
//  PlayListCoordinator.swift
//  POCDeezer
//
//  Created by mac on 13/01/1401 AP.
//

import Foundation

class PlayListCoordinator: BaseCoordinator {
    
    private let viewModel: PlayListViewModel
    
    init(viewModel: PlayListViewModel) {
        self.viewModel = viewModel
    }
    
    override func start() {
        let viewController = PlayListViewController.instantiate()
        viewController.viewModel = viewModel
        navigationController.pushViewController(viewController, animated: true)
    }
    
    
}
