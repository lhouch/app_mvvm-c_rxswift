//
//  PlayListViewModel.swift
//  POCDeezer
//
//  Created by mac on 13/01/1401 AP.
//

import RxSwift
import UIKit

class PlayListViewModel {
    
    fileprivate let disposeBag = DisposeBag()
    private let mPOCDeezerService: POCDeezerAPI
    let selectPlayList: Playlist?
    let isLoading = BehaviorSubject(value: true)
    let playList = PublishSubject<PlaylistInfoResponse>()
    let tracks = PublishSubject<TracksResponse>()
    let responseError = PublishSubject<Error>()
    
    
    
    
    
    init(service: POCDeezerAPI, playList: Playlist) {
        self.mPOCDeezerService = service
        self.selectPlayList = playList
        getPlayListInfo()
        getTracks()
    }
    
    func getPlayListInfo() -> Void {
        isLoading.onNext(true)
        self.mPOCDeezerService
            .fetchPlayListInfo(playListId:
                                String(describing: self.selectPlayList?.id ?? 0))
            .subscribe { result in
                self.playList.onNext(result)
            } onError: { error in
                self.responseError.onNext(error)
            }
            .disposed(by: disposeBag)
    }
    
    func getTracks() -> Void {
        isLoading.onNext(true)
        self.mPOCDeezerService
            .fetchTracks(playListId:
                            String(describing: self.selectPlayList?.id ?? 0))
            .subscribe { result in
                self.tracks.onNext(result)
            }onError: { error in
                self.responseError.onNext(error)
            }
            .disposed(by: disposeBag)
    }
    
    
}
