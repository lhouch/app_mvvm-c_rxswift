//
//  PlayListViewController.swift
//  POCDeezer
//
//  Created by mac on 13/01/1401 AP.
//

import UIKit
import RxSwift

class PlayListViewController: UIViewController, Storyboarded {
    static var storyboard = AppStoryboard.main
    
    let maxHeaderHeight: CGFloat = 310
    let minHeaderHeight: CGFloat = 170
    var previousScrollOffset: CGFloat = 0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var playListNameLbl: UILabel!
    
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var creatorLbl: UILabel!
    var viewModel: PlayListViewModel?
    private let disposeBag = DisposeBag()
    
    var tracks = [Track]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        let cell = UINib(nibName: "PlayListTableViewCell", bundle: nil)
        tableView.register(cell, forCellReuseIdentifier: "myCell")
        
        self.title = ""
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        setUpBindings()
    }
    
    func initUI(_ playListInfo: PlaylistInfoResponse) {
        
        if let imageUrl = playListInfo.picture_medium {
            imageView.load(url: URL(string: imageUrl)!)
        }
        
        playListNameLbl.text = playListInfo.title
        creatorLbl.text = playListInfo.creator?.name
        durationLbl.text = "\(playListInfo.duration ?? 0)"
        
        durationLbl.text = playListInfo.duration!.secondsToHoursMinutesSeconds()
    }
    
    
    private func setUpBindings() {
        
        viewModel?.playList
            .subscribe { result in
                
                self.initUI(result.element!)
                self.hideActivityIndicatoryInSuperview()
                
            }.disposed(by: disposeBag)
        
        
        viewModel?.tracks
            .subscribe { result in
                if let tracks = result.element?.data, !tracks.isEmpty {
                    self.tracks = tracks
                    self.tableView.reloadData()
                } else {
                    self.showAlertViewInSuperview("Error", message: "No data to show") {
                        print("No data to show")
                    }
                }
                
                self.hideActivityIndicatoryInSuperview()
                
            }.disposed(by: disposeBag)
        
        viewModel?.responseError.subscribe{error in
            self.hideActivityIndicatoryInSuperview()            
            self.showAlertViewInSuperview("Error", message: error.element!.localizedDescription) {
                print(error)
            }
        }.disposed(by: disposeBag)
        
        viewModel?.isLoading
            .subscribe { isLoading in
                if isLoading.element! {
                    self.showActivityIndicatoryInSuperview()
                }
            }.disposed(by: disposeBag)
        
    }
    
}


extension PlayListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tracks.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! PlayListTableViewCell
        
        let track = self.tracks[indexPath.row]
        cell.configCell(track)
        
        return cell
    }
}
extension PlayListViewController {
    func canAnimateHeader (_ scrollView: UIScrollView) -> Bool {
        let scrollViewMaxHeight = scrollView.frame.height + self.headerViewHeight.constant - minHeaderHeight
        return scrollView.contentSize.height > scrollViewMaxHeight
    }
    func setScrollPosition() {
        self.tableView.contentOffset = CGPoint(x:0, y: 0)
    }
}
extension PlayListViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = (scrollView.contentOffset.y - previousScrollOffset)
        let isScrollingDown = scrollDiff > 0
        let isScrollingUp = scrollDiff < 0
        if canAnimateHeader(scrollView) {
            var newHeight = headerViewHeight.constant
            if isScrollingDown {
                newHeight = max(minHeaderHeight, headerViewHeight.constant - abs(scrollDiff))
            } else if isScrollingUp {
                newHeight = min(maxHeaderHeight, headerViewHeight.constant + abs(scrollDiff))
            }
            if newHeight != headerViewHeight.constant {
                headerViewHeight.constant = newHeight
                setScrollPosition()
                previousScrollOffset = scrollView.contentOffset.y
            }
        }
    }
}
