//
//  PlayListTableViewCell.swift
//  POCDeezer
//
//  Created by mac on 13/01/1401 AP.
//

import UIKit

class PlayListTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    
    @IBOutlet weak var viewContent: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configUI()
    }

    
    func configCell(_ track: Track) {
        self.titleLbl.text = track.title
        self.descLbl.text = track.artist?.name
        self.durationLbl.text = track.duration?.secondsToHoursMinutesSeconds()
    }
    
    func configUI(){
        self.viewContent.layer.cornerRadius = 5
        self.viewContent.layer.borderColor = UIColor.lightGray.cgColor
        self.viewContent.layer.borderWidth = 1
        self.viewContent.layer.masksToBounds = true
     }
    
}
