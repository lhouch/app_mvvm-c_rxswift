//
//  POCDeezerService.swift
//  POCDeezer
//
//  Created by mac on 12/01/1401 AP.
//

import RxSwift
import Alamofire

class POCDeezerService {
    private lazy var httpService = POCDeezerHttpService()
    static let shared: POCDeezerService = POCDeezerService()
}

extension POCDeezerService: POCDeezerAPI {
    
    
    func fetchTracks(playListId: String) -> Single<TracksResponse> {
        return Single.create { [httpService] (single) -> Disposable in
            
            do {
                try POCDeezerHttpRouter.getTracks(playListId)
                    .request(usingHttpService: httpService)
                    .responseJSON { (result) in
                        do {
                            let tracks = try POCDeezerService.parseTracks(result: result)
                            single(.success(tracks))
                        } catch {
                            single(.error(error))
                        }
                    }
            } catch {
                single(.error(CustomError.error(message: "Tracks fetch failed")))
            }
            
            return Disposables.create()
        }
    }
    
    
    func fetchPlayListInfo(playListId: String) -> Single<PlaylistInfoResponse> {
        return Single.create { [httpService] (single) -> Disposable in
            
            do {
                try POCDeezerHttpRouter.getPlayListInfo(playListId)
                    .request(usingHttpService: httpService)
                    .responseJSON { (result) in
                        do {
                            let playListInfo = try POCDeezerService.parsePlayListInfo(result: result)
                            single(.success(playListInfo))
                        } catch {
                            single(.error(error))
                        }
                    }
            } catch {
                single(.error(CustomError.error(message: "PlayList fetch failed")))
            }
            
            return Disposables.create()
        }
    }
    
    
    
    func fetchPlayLists() -> Single<PlaylistsResponse> {
        return Single.create { [httpService] (single) -> Disposable in
            
            do {
                try POCDeezerHttpRouter.getPlayLists
                    .request(usingHttpService: httpService)
                    .responseJSON { (result) in
                        do {
                            let playLists = try POCDeezerService.parsePlayLists(result: result)
                            single(.success(playLists))
                        } catch {
                            single(.error(error))
                        }
                    }
            } catch {
                single(.error(CustomError.error(message: "PlayLists fetch failed")))
            }
            
            return Disposables.create()
        }
    }
}

extension POCDeezerService {
    static func parsePlayLists(result: AFDataResponse<Any>) throws -> PlaylistsResponse {
        
        guard
            let data = result.data,
            let playListsResponse = try? JSONDecoder().decode(PlaylistsResponse.self, from: data)
        else {
            throw CustomError.error(message: "Invalid Data")
        }
        return playListsResponse
    }
    
    static func parsePlayListInfo(result: AFDataResponse<Any>) throws -> PlaylistInfoResponse {
        
        guard
            let data = result.data,
            let playListInfoResponse = try? JSONDecoder().decode(PlaylistInfoResponse.self, from: data)
        else {
            throw CustomError.error(message: "Invalid Data")
        }
        return playListInfoResponse
    }
    
    static func parseTracks(result: AFDataResponse<Any>) throws -> TracksResponse {
        
        guard
            let data = result.data,
            let tracks = try? JSONDecoder().decode(TracksResponse.self, from: data)
        else {
            throw CustomError.error(message: "Invalid Data")
        }
        return tracks
    }
    
}
