//
//  POCDeezerHttpRouter.swift
//  POCDeezer
//
//  Created by mac on 12/01/1401 AP.
//

import Alamofire

/*****************API Key***********************/
let  USER_ID = "2529"
let  LIST_PLAYLISTS_URL = "user/\(USER_ID)/playlists"
let  LIST_PLAYLIST_INFO_URL = "playlist"
let  LIST_TRACKS_URL = "playlist/%@/tracks"



enum POCDeezerHttpRouter {
    case getPlayLists
    case getPlayListInfo(String)
    case getTracks(String)
}

extension POCDeezerHttpRouter: HttpRouter{
    
    var baseUrlString: String {
        return Bundle.main.object(forInfoDictionaryKey: "BaseUrL") as! String
    }
    
    var path: String {
        switch self {
        case .getPlayLists:
            return LIST_PLAYLISTS_URL
        case .getPlayListInfo(let playlistId):
            return "\(LIST_PLAYLIST_INFO_URL)/\(playlistId)"
        case .getTracks(let playlistId):
            return String(format: LIST_TRACKS_URL, playlistId)
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getPlayLists, .getPlayListInfo, .getTracks:
            return .get
        }
    }
    
    var parammeters: Parameters? {
        switch self {
        case .getPlayLists, .getPlayListInfo, .getTracks:
            return nil
        }
    }
    
    func body() throws -> Data? {
        return nil
    }
}
