//
//  HttpService.swift
//  POCDeezer
//
//  Created by mac on 12/01/1401 AP.
//

import Alamofire

protocol HttpService {
    var sessionManager: Session {get set}
    func request(_ urlRequest: URLRequestConvertible) -> DataRequest
}
