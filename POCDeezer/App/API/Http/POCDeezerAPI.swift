//
//  POCDeezerAPI.swift
//  POCDeezer
//
//  Created by mac on 12/01/1401 AP.
//

import RxSwift

protocol POCDeezerAPI {
    func fetchPlayLists() -> Single<PlaylistsResponse>
    func fetchPlayListInfo(playListId: String) -> Single<PlaylistInfoResponse>
    func fetchTracks(playListId: String) -> Single<TracksResponse>

}
