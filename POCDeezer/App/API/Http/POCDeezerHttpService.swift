//
//  POCDeezerHttpService.swift
//  POCDeezer
//
//  Created by mac on 12/01/1401 AP.
//

import Alamofire

class POCDeezerHttpService: HttpService {
  
    var sessionManager: Session = Session.default
    func request(_ urlRequest: URLRequestConvertible) -> DataRequest {
        return sessionManager.request(urlRequest).validate(statusCode: 200..<400)
    }
}
