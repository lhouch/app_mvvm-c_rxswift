//
//  POCDeezerTests.swift
//  POCDeezerTests
//
//  Created by mac on 10/01/1401 AP.
//

import XCTest
@testable import POCDeezer
import RxSwift

class POCDeezerTests: XCTestCase {
    

    private var disposeBag:DisposeBag?
    private var homeViewModel: HomeViewModel?
    private var playListViewModel: PlayListViewModel?
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    override func setUp() {
        super.setUp()
        
        self.disposeBag = DisposeBag()
        let apiClientShared = POCDeezerService.shared

        
        // Init HomeViewModel get playLists
        self.homeViewModel = HomeViewModel(service: apiClientShared)
        self.homeViewModel?.getPlayLists()

        // Init PlayListViewModel get tracks
        let playListExmple = Playlist(id: 2896937, title: "a decouvrir", pictureMedium: "")
        self.playListViewModel = PlayListViewModel(service: apiClientShared, playList: playListExmple)
        self.playListViewModel?.getTracks()
        
        
        self.setUpBindings()
        
    }
    
    
    private func setUpBindings() {
        
        self.homeViewModel?.playListsResponse
            .subscribe { result in
                    print(result)
            } onError: { error in
                print(error.localizedDescription)
            }.disposed(by: self.disposeBag!)
  
        
        self.playListViewModel?.playList
            .subscribe { result in
                
                print(result)
                
            } onError: { error in
                print(error)

            }.disposed(by: self.disposeBag!)
        
        
        self.playListViewModel?.tracks
            .subscribe { result in
                
                print(result)

                
            } onError: { error in
                print(error.localizedDescription)

            }.disposed(by: self.disposeBag!)
       
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
